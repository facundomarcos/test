# GIT y herramientas para implementar el control de versiones en aplicaciones para desarrolladores/as

Este repositorio contiene la actividad integradora del curso de GIT y herramientas para implementar el control de versiones en aplicaciones para desarrolladores/as dictado por el IPAP. 
El objetivo del curso es proporcionar a los estudiantes una comprensión de las funciones fundamentales de los versionadores. 

## Proyecto de clon del Estado Argentino

Este proyecto consiste en crear un clon de un proyecto del Estado Argentino. El objetivo es disponer en nuestro proyecto de las características visuales que se utilizan en los proyectos del Estado Argentino.

El repositorio contiene todos los archivos necesarios para el proyecto, incluyendo los archivos HTML, CSS y JavaScript, así como las imágenes y otros recursos utilizados en el sitio web. Los contribuyentes pueden trabajar en el proyecto y hacer mejoras para asegurar que la versión clonada sea lo más precisa y actualizada posible.


#Aquí se presentan dos herramientas web para aprender ramas en Git:

Git Branching: Esta es una herramienta interactiva en línea que te ayuda a aprender los conceptos básicos de las ramas en Git. Te guía a través de diferentes escenarios y te permite crear, fusionar y eliminar ramas para que puedas experimentar con diferentes situaciones. También hay una sección de tutoriales que cubre temas más avanzados. Puedes acceder a Git Branching en el siguiente enlace: https://learngitbranching.js.org/

Codecademy: Codecademy es una plataforma de aprendizaje en línea que ofrece una variedad de cursos de programación, incluyendo uno dedicado a Git. En el curso de Git, aprenderás cómo usar ramas para organizar tu trabajo y colaborar con otros en proyectos de Git. El curso es interactivo y te guía a través de diferentes escenarios y ejercicios prácticos. Puedes acceder al curso de Git en el siguiente enlace: https://www.codecademy.com/learn/learn-git